#pragma once

#include <memory>
#include <vector>
#include<map>
#include <utility>
#include <string>

#include "static/dependency.hpp"


using std::map;
using std::shared_ptr;
using std::string;
using std::vector;


class Node : public std::enable_shared_from_this<Node> {
public:
    static NodePtr node(string name, const vector<DependencyPtr>& dependencies);
    static NodePtr node(string name, vector<DependencyPtr>&& dependencies);
    static NodePtr node(string name);
    Node(string name): _name(name), _dependencies() {};
    Node(string name, const vector<DependencyPtr>& dependencies): _name(name), _dependencies(dependencies) {};
    Node(string name, vector<DependencyPtr>&& dependencies): _name(name), _dependencies(std::move(dependencies)) {};
    string name() const;
    void name(string value);
    vector<shared_ptr<const Dependency>> dependencies() const;
    const map<string, NodePtr>& dependents() const;
    void add_dependent(NodePtr node);
    vector<NodePtr> get_ordered_graph();
    string repr() const;
protected:
    void _add_as_dependent();
    string _name;
    vector<DependencyPtr> _dependencies;
    map<string, NodePtr> _dependents;
};

