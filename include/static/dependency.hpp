#pragma once

#include <memory>
#include <vector>

using std::shared_ptr;

class Node;
class Dependency;

using DependencyPtr = shared_ptr<Dependency>;
using NodePtr = shared_ptr<Node>;

class Dependency {
public:
    enum class Type {SIMPLE, OPTIONAL, ORDER};
    explicit Dependency(NodePtr node, Type type = Type::SIMPLE): _node(node), _type(type) {};
    NodePtr node() const;
    Type type();
private:
    NodePtr _node;
    Type _type;
};
