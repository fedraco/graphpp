#include <iostream>
#include <memory>
#include <string>
#include <utility>
#include <memory>

#include "static/dependency.hpp"
#include "static/graph.hpp"

using std::cout;
using std::endl;
using std::make_shared;

int main(int argc, char** argv) {
    NodePtr leaf1 = Node::node("leaf1");
    NodePtr leaf2 = Node::node("leaf2");
    NodePtr some_node = Node::node("some_node", vector<DependencyPtr>{
	    make_shared<Dependency>(leaf1),
	    make_shared<Dependency>(leaf2)
	});
    NodePtr some_upper_node = Node::node("some_upper_node", vector<DependencyPtr>{
	    make_shared<Dependency>(leaf2),
	    make_shared<Dependency>(some_node)
	});
    auto ordered_nodes = some_upper_node->get_ordered_graph();
    cout << "Ordered nodes: [ ";
    for(auto node : ordered_nodes) {
	cout << node->name() << " ";
    }
    cout << "]" << endl;
    return 0;
}
