#include <tuple>
#include <cstddef>
#include <deque>
#include <set>
#include <map>
#include <utility>
#include <memory>
#include <string>

#include "static/dependency.hpp"
#include "static/graph.hpp"


using std::deque;
using std::map;
using std::set;
using std::string;


NodePtr Node::node(string name, const vector<DependencyPtr>& dependencies) {
    NodePtr _node = std::make_shared<Node>(name, dependencies);
    _node->_add_as_dependent();
    return _node;
}

NodePtr Node::node(string name, vector<DependencyPtr>&& dependencies) {
    NodePtr _node = std::make_shared<Node>(name, std::move(dependencies));
    _node->_add_as_dependent();
    return _node;
}

NodePtr Node::node(string name) {
    NodePtr _node = std::make_shared<Node>(name);
    _node->_add_as_dependent();
    return _node;
}

string Node::name() const { return this->_name; }

void Node::name(string value) { this->_name = value; }

vector<shared_ptr<const Dependency>> Node::dependencies() const {
  return {this->_dependencies.begin(), this->_dependencies.end()};
}

vector<NodePtr> Node::get_ordered_graph() {
    struct NodeMetadata {
	std::size_t num_of_nonvisited_dependents;
	bool visited;
    };
    map<string, NodeMetadata> nodes_metadata;
    set<string> visited_nodes;
    deque<NodePtr> node_queue;
    vector<NodePtr> result{shared_from_this()};
    for(auto dep : this->_dependencies) {
	auto _cur_node = dep->node();
	if(_cur_node->dependents().size() == 1) {
	    nodes_metadata[_cur_node->name()] = NodeMetadata{0, true};
	    node_queue.push_back(_cur_node);
	} else {
	    nodes_metadata[_cur_node->name()] = NodeMetadata{_cur_node->dependents().size()-1, false};
	}
    }
    while(!node_queue.empty()) {
	auto _node = node_queue.front();
	node_queue.pop_front();
	result.push_back(_node);
	for(auto dep : _node->dependencies()) {
	    auto _dep_node = dep->node();
	    auto _dep_metadata_iter = nodes_metadata.find(_dep_node->name());
	    if(_dep_metadata_iter != nodes_metadata.end()) {
		_dep_metadata_iter->second.num_of_nonvisited_dependents -= 1;
	    } else {
		nodes_metadata[_dep_node->name()] = NodeMetadata{_dep_node->dependents().size() - 1, false};
		std::tie(_dep_metadata_iter, std::ignore) = nodes_metadata.insert({_dep_node->name(), NodeMetadata{_dep_node->dependents().size() - 1, false}});
	    }
	    if(_dep_metadata_iter->second.num_of_nonvisited_dependents == 0) {
		node_queue.push_back(_dep_node);
	    }
	}
    }
    return result;
}

const map<string, NodePtr> &Node::dependents() const {
  return this->_dependents;
}

void Node::add_dependent(NodePtr node) {
    this->_dependents[node->name()] = node;
}

string Node::repr() const {
    string _repr = "- " + this->name() + " {\n";
    for (auto dependency : this->dependencies()) {
	_repr += dependency->node()->repr();
    }
    _repr += "\n}";
    return _repr;
}

void Node::_add_as_dependent() {
    for(auto dep : this->_dependencies) {
	dep->node()->add_dependent(shared_from_this());
    }
}
