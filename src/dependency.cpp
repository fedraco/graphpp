#include "static/dependency.hpp"
#include "static/graph.hpp"
#include <memory>

using std::shared_ptr;

shared_ptr<Node> Dependency::node() const { return this->_node; }

Dependency::Type Dependency::type() { return this->_type; }

